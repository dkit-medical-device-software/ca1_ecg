"""
CA1 ECG analysis

Student name: 
"""

import sys
# imports go here (anything in numpy/scipy is fine)

# re-usable functions / classes go here

def analyse_ecg(signal_filename, annotations_filename):
    # basic formatted output in Python
    print('signal filename: %s' % signal_filename)
    print('annotation filename: %s' % annotation_filename)
    
    # your analysis code here    

if __name__=='__main__':
    analyse_ecg(sys.argv[1] + '_signals.txt', sys.argv[1]+'_annotations.txt')

